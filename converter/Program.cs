﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace converter
{
    class Program
    {
        //I defined every array and list in the class to make easy to work within multiple functions.
        public static string[] main;
        static List<string> finalNames = new List<string>();
        static List<string> finalNumbers = new List<string>();
        
        
        //Main class which reads the file.
        //Will currently make code only file readings, maybe later add a form to ask for a file location.
        static void Main(string[] args)
        {
            //This reads the file.
            main = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "\\backup.csv");
            //Some variables for organizing the loop.
            int count = 0;
            int count2 = 0;
            int finalCount = 0;
            //This loop choses what to save from the file has the file have multiple useless lines.
            for (int i = 0; i < main.Length; i++)
            {

                if (count == 3)
                {
                    if (finalCount + 1 < main.Length)
                    {
                        finalNames.Add(main[i]);
                        finalNumbers.Add(main[i + 1]);


                        count = -1;
                        i = i + 3;
                        finalCount++;
                    }
                }
                count++;
            }
            //This loop shows what was saved.
            for (int i = 0; i < finalNames.Count; i++) {
                Console.WriteLine(finalNames[i] + " " + finalNumbers[i]);
            }
            Console.ReadLine();
            namesWork();
            numbersWork();
            finalFileWriting();
        }

        //This function removes the ;;; after every name.
        public static void namesWork()
        {
            string tmp;
            for (int i = 0; i < finalNames.Count; i++)
            {
                tmp = finalNames[i];
                tmp = Reverse(tmp);
                tmp = tmp.Remove(0, 3);
                tmp = Reverse(tmp);
                finalNames[i] = tmp;

                Console.WriteLine(finalNames[i]);
            }
            Console.ReadLine();
        }
        //This function removes the text that is before every number.
        public static void numbersWork()
        {
            int count = 0;
            for (int i = 0; i < finalNumbers.Count; i++)
            {
                count = 0;
                foreach(char c in finalNumbers[i]){
                    if(c == ':'){
                        finalNumbers[i] = finalNumbers[i].Remove(0,count+1);
                        Console.WriteLine(finalNumbers[i]);
                    }
                count++;
                }
            }
            Console.ReadLine();
        }
        //This is the fuction where the final .csv is written.
        public static void finalFileWriting()
        {
            List<string> final = new List<string>();
            for(int i = 0; i < finalNames.Count; i++){
                final.Add(finalNames[i] + ";" + finalNumbers[i]);
            }
            File.WriteAllLines(AppDomain.CurrentDomain.BaseDirectory + "\\final.csv", final);
        }

        //Simple function to reverse a string.
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
