**NokiaToNew Contacts Backup Converter**

This simple algorithm gets your old nokia contacts backup and turns it into a simple .csv file for reading on newer devices.

---

## Original Repository
[link](https://bitbucket.org/iuribdias/ntncontactconverter)

